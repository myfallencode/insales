import Vue from 'vue'
import router from './router'
import App from './App.vue'
import './assets/css/common.css'
//import 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'

Vue.use({ router: router });
Vue.config.productionTip = false

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')