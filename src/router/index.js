import Vue from 'vue'
import Router from 'vue-router'

//Common
import index from '../components/index.vue'

Vue.use(Router);

let router = new Router({
    mode: 'history',
    routes: [{
        path: '/',
        name: 'index',
        components: {
            main: index,
        }
    }]
});


export default router;